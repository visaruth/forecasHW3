isPure <- function(data){
  length(unique(data[,1])) == 1
}